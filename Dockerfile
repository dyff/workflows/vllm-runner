FROM vllm/vllm-openai:v0.7.2

ENV PYTHONDONTWRITEBYTECODE="1" \
    PYTHONUNBUFFERED="1"

# hadolint ignore=DL3013
RUN python3 -m pip install --no-cache-dir --upgrade pip setuptools wheel

WORKDIR /dyff/app/

COPY entrypoint.sh /dyff/app/entrypoint.sh
RUN chmod 755 /dyff/app/entrypoint.sh

# Default server port
EXPOSE 8000

ENTRYPOINT ["/dyff/app/entrypoint.sh"]
