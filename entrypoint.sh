#!/bin/bash
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

if [[ -z "${DYFF_INFERENCESESSIONS__MULTI_NODE}" ]]; then
    exec python3 -m vllm.entrypoints.openai.api_server --host "0.0.0.0" --port "8000" "$@"
else
    if [[ -z "${DYFF_INFERENCESESSIONS__POD_INDEX}" ]]; then
        exit "DYFF_INFERENCESESSIONS__POD_INDEX not set"
    fi

    ray_port="6739"

    if [[ "${DYFF_INFERENCESESSIONS__POD_INDEX}" == "0" ]]; then
        # This is the "leader" pod
        ray start --head --port="${ray_port}"
        # This blocks forever
        python3 -m vllm.entrypoints.openai.api_server --host "0.0.0.0" --port "8000" "$@"
    else
        # This is a "worker" pod
        if [[ -z "${DYFF_INFERENCESESSIONS__LEADER_HOST}" ]]; then
            exit "DYFF_INFERENCESESSIONS__LEADER_HOST not set"
        fi

        # --block keeps the process alive
        ray start --block --address="${DYFF_INFERENCESESSIONS__LEADER_HOST}:${ray_port}"
    fi
fi
