# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# This code is based on code from the vLLM library,
#   https://github.com/vllm-project/vllm
# available under the following license (Note: there is no copyright date or
# copyright holder specified on the project website as of 2024/06/18):
#
# Copyright [yyyy] [name of copyright owner]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

"""API server backed by the vLLM engine.

This is very close to the two different servers implemented in vllm.entrypoints,
but we make some modifications to suit the Dyff use case. Specifically:

    * We implement a /ready endpoint that can be polled to see if the engine
      has finished loading.
    * We wrap all of the inference endpoints in a ready check, so that they
      will return 503 if the engine is still loading, or 500 if the engine
      encountered a permanent error.
    * We run the server in a separate thread so that it is responsive while
      waiting for the engine to load.
    * We provide only a subset of the command-line arguments, because we don't
      want users messing with SSL config and whatnot.
"""

import argparse
import asyncio
import json
import threading
import traceback
from typing import AsyncGenerator, Optional

import fastapi
import uvicorn
import vllm.entrypoints.openai.api_server as vllm_openai
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, Response, StreamingResponse
from vllm.engine.arg_utils import AsyncEngineArgs, nullable_str
from vllm.engine.async_llm_engine import AsyncLLMEngine
from vllm.entrypoints.openai.cli_args import LoRAParserAction
from vllm.entrypoints.openai.serving_chat import OpenAIServingChat
from vllm.entrypoints.openai.serving_completion import OpenAIServingCompletion
from vllm.entrypoints.openai.serving_embedding import OpenAIServingEmbedding
from vllm.logger import init_logger
from vllm.sampling_params import SamplingParams
from vllm.utils import random_uuid

TIMEOUT_KEEP_ALIVE = 5  # seconds.
TIMEOUT_TO_PREVENT_DEADLOCK = 1  # seconds.
app = FastAPI()
_engine_error = threading.Event()
_engine_ready = threading.Event()
engine: Optional[AsyncLLMEngine] = None
error = None
logger = init_logger(__name__)


def check_engine_status() -> AsyncLLMEngine:
    if _engine_error.is_set():
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error
        )
    if not _engine_ready.is_set():
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_503_SERVICE_UNAVAILABLE
        )
    if engine is None:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="engine_ready=True but engine is None",
        )
    return engine


# vllm_openai.app is a full FastAPI app, but we want to treat it as just an
# APIRouter so that we can add global dependencies.
app.include_router(
    vllm_openai.app.router,
    prefix="/openai",
    dependencies=[fastapi.Depends(check_engine_status)],
)


@app.get("/health")
async def health() -> Response:
    """Health check."""
    return Response(status_code=fastapi.status.HTTP_200_OK)


@app.get("/ready")
async def ready() -> Response:
    check_engine_status()
    return Response(status_code=fastapi.status.HTTP_200_OK)


@app.post("/generate")
async def generate(
    request: Request, engine=fastapi.Depends(check_engine_status)
) -> Response:
    """Generate completion for the request.

    The request should be a JSON object with the following fields:
    - prompt: the prompt to use for the generation.
    - stream: whether to stream the results or not.
    - other fields: the sampling parameters (See `SamplingParams` for details).
    """

    request_dict = await request.json()
    prompt = request_dict.pop("prompt")
    stream = request_dict.pop("stream", False)
    sampling_params = SamplingParams(**request_dict)
    request_id = random_uuid()

    results_generator = engine.generate(prompt, sampling_params, request_id)

    # Streaming case
    async def stream_results() -> AsyncGenerator[bytes, None]:
        async for request_output in results_generator:
            prompt = request_output.prompt
            text_outputs = [prompt + output.text for output in request_output.outputs]
            ret = {"text": text_outputs}
            yield (json.dumps(ret) + "\0").encode("utf-8")

    if stream:
        return StreamingResponse(stream_results())

    # Non-streaming case
    final_output = None
    async for request_output in results_generator:
        if await request.is_disconnected():
            # Abort the request if the client disconnects.
            await engine.abort(request_id)
            return Response(status_code=499)
        final_output = request_output

    assert final_output is not None
    prompt = final_output.prompt
    text_outputs = [prompt + output.text for output in final_output.outputs]
    ret = {"text": text_outputs}
    return JSONResponse(ret)


def _init_openai_server(engine: AsyncLLMEngine, args):
    event_loop: Optional[asyncio.AbstractEventLoop]
    try:
        event_loop = asyncio.get_running_loop()
    except RuntimeError:
        event_loop = None

    if event_loop is not None and event_loop.is_running():
        # If the current is instanced by Ray Serve,
        # there is already a running event loop
        model_config = event_loop.run_until_complete(engine.get_model_config())
    else:
        # When using single vLLM without engine_use_ray
        model_config = asyncio.run(engine.get_model_config())

    if args.served_model_name is not None:
        served_model_names = args.served_model_name
    else:
        served_model_names = [args.model]

    vllm_openai.openai_serving_chat = OpenAIServingChat(
        engine,
        model_config,
        served_model_names,
        args.response_role,
        args.lora_modules,
        args.chat_template,
    )
    vllm_openai.openai_serving_completion = OpenAIServingCompletion(
        engine, model_config, served_model_names, args.lora_modules
    )
    vllm_openai.openai_serving_embedding = OpenAIServingEmbedding(
        engine, model_config, served_model_names
    )


def _load_engine(args):
    global engine, error, _engine_error, _engine_ready
    try:
        engine_args = AsyncEngineArgs.from_cli_args(args)
        engine = AsyncLLMEngine.from_engine_args(engine_args)
        _init_openai_server(engine, args)
        _engine_ready.set()
    except Exception:
        error = traceback.format_exc()
        logger.exception("failed to create LLMEngine")
        _engine_error.set()


def _server_thread(host, port, log_level):
    uvicorn.run(
        app,
        host=host,
        port=port,
        log_level=log_level,
        timeout_keep_alive=TIMEOUT_KEEP_ALIVE,
    )


def main():
    parser = argparse.ArgumentParser()
    # Server args
    parser.add_argument("--host", type=str, default=None)
    parser.add_argument("--port", type=int, default=8000)
    # OpenAI backend args
    parser.add_argument(
        "--lora-modules",
        type=nullable_str,
        default=None,
        nargs="+",
        action=LoRAParserAction,
        help="LoRA module configurations in the format name=path. "
        "Multiple modules can be specified.",
    )
    parser.add_argument(
        "--chat-template",
        type=nullable_str,
        default=None,
        help="The file path to the chat template, "
        "or the template in single-line form "
        "for the specified model",
    )
    parser.add_argument(
        "--response-role",
        type=nullable_str,
        default="assistant",
        help="The role name to return if " "`request.add_generation_prompt=true`.",
    )
    # vLLM engine args
    parser = AsyncEngineArgs.add_cli_args(parser)
    args = parser.parse_args()

    server_thread = threading.Thread(
        target=_server_thread, args=(args.host, args.port, "debug")
    )
    server_thread.start()

    _load_engine(args)

    server_thread.join()


if __name__ == "__main__":
    main()
